{{ name }}
===========
{{ description }}

## Packages
{% for package in packages %}
- [{{ package.name }}]({{ package.name }}/) - {{ package.description }}
{% for function in package.functions %}
    * [{{ function.name }}]({{ package.name }}/{{ function.name }}) - {{ package.description }}
{% endfor %}
{% endfor %}
