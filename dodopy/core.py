import logging
import os
import subprocess
import json
from urllib.parse import urljoin
import requests
import digitalocean
from jinja2 import Template

from dodopy import settings

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
here = os.path.dirname(os.path.abspath(__file__))
manager = digitalocean.Manager(token=settings.ACCESS_TOKEN)

TEMPLATE_DIR = os.path.join(here, "templates")


class DoFunctionsAPI(object):
    session = requests.Session()
    session.headers.update(
        {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {settings.ACCESS_TOKEN}",
        }
    )

    def _endpoint(self, path):
        """Build the full URL for a given path"""
        return urljoin(settings.BASE_URL, path)

    def _request(self, method, path, **kwargs):
        """Make a request to the Digital Ocean API"""
        r = self.session.request(method, self._endpoint(path), **kwargs)
        r.raise_for_status()
        if method != "DELETE":
            return r.json()
        else:
            return True

    def list_namespaces(self):
        """List all namespaces"""
        return self._request("GET", "functions/namespaces").get("namespaces", [])

    def create_namespace(self, name, region="nyc3"):
        """Create a namespace with the given name and region"""
        return self._request(
            "POST", "functions/namespaces", json={"name": name, "region": region}
        ).get("namespace", {})

    def get_namespace(self, name):
        """Get a namespace by name"""
        return self._request("GET", f"functions/namespaces/{name}").get("namespace", {})

    def delete_namespace(self, namespace_id):
        """Delete a namespace by id"""
        return self._request("DELETE", f"functions/namespaces/{namespace_id}")

    def list_triggers(self, namespace_id):
        """List all triggers"""
        return self._request(
            "GET", f"functions/namespaces/{namespace_id}/triggers"
        ).get("triggers", [])

    def create_trigger(
        self, namespace_id, name, function, cron, body={}, is_enabled=True
    ):
        """Create a trigger with the given name, function, cron, and body"""
        return self._request(
            "POST",
            f"functions/namespaces/{namespace_id}/triggers",
            json={
                "name": name,
                "function": function,
                "type": "SCHEDULED",
                "scheduled_details": {
                    "cron": cron,
                    "body": body,
                },
                "is_enabled": is_enabled,
            },
        ).get("trigger", {})

    def get_trigger(self, namespace_id, trigger_name):
        """Get a trigger by name"""
        return self._request(
            "GET", f"functions/namespaces/{namespace_id}/triggers/{trigger_name}"
        ).get("trigger", {})

    def update_trigger(self, namespace_id, trigger_name, cron, body, is_enabled=True):
        """Update a trigger by name"""
        return self._request(
            "PUT",
            f"functions/namespaces/{namespace_id}/triggers/{trigger_name}",
            json={
                "scheduled_details": {
                    "cron": cron,
                    "body": body,
                },
                "is_enabled": is_enabled,
            },
        ).get("trigger", {})

    def delete_trigger(self, namespace_id, trigger_name):
        """Delete a trigger by name"""
        return self._request(
            "DELETE", f"functions/namespaces/{namespace_id}/triggers/{trigger_name}"
        )


class DoServerlessAPI(object):
    """
    A wrapper for doctl serverless function commands
    """

    def exec(self, *args, **kwargs):
        flags = []
        for k, v in kwargs.items():
            if not v:
                continue
            k = k.replace("_", "-")
            if isinstance(v, bool):
                if v:
                    flags.append(f"--{k}")
            else:
                flags.append(f'--{k}="{v}"')
        args = [str(arg) for arg in args if arg]
        cmd = [
            settings.DOCTL_EXECUTABLE,
            "-t",
            settings.ACCESS_TOKEN,
            "-o",
            "json",
            "serverless",
            *args,
            *flags,
        ]
        try:
            # execute subprocess and log stderr to logger if it exists
            p = subprocess.Popen(
                cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
            )
            result, err = p.communicate()
            if err:
                log.error(err.decode("utf-8"))
                return {"message": err.decode("utf-8"), "status": "ERROR"}
            try:
                return json.loads(result.decode("utf-8"))
            except json.JSONDecodeError:
                return {"message": result.decode("utf-8"), "status": "OK"}
        except subprocess.CalledProcessError as e:
            return {"message": e.output.decode("utf-8"), "status": "ERROR"}


    def install(self):
        """
        Install the doctl serverless plugin
        """
        return self.exec("install")

    def upgrade(self):
        """
        Upgrade the doctl serverless plugin
        """
        return self.exec("upgrade")

    def connect(self, namespace=""):
        """
        This command connects `doctl serverless` support to a functions namespace of your choice.
        The optional argument should be a (complete or partial) match to a namespace label or id.
        If there is no argument, all namespaces are matched.  If the result is exactly one namespace,
        you are connected to it.  If there are multiple namespaces, you have an opportunity to choose
        the one you want from a dialog.  Use `doctl serverless namespaces` to create, delete, and
        list your namespaces.
        """
        return self.exec("connect", namespace)

    def undeploy(self, **kwargs):
        # This command removes functions, entire packages, or all functions and packages, from your function
        # namespace.  In general, deploying new content does not remove old content although it may overwrite it.
        # Use `doctl serverless undeploy` to effect removal.  The command accepts a list of functions or packages.
        # Functions should be listed in `pkgName/fnName` form, or `fnName` for a function not in any package.
        # The `--packages` flag causes arguments without slash separators to be interpreted as packages, in which case
        # the entire packages are removed.

        # Usage:
        #   doctl serverless undeploy [<package|function>...] [flags]

        # Flags:
        #       --all        remove all packages and functions
        #   -h, --help       help for undeploy
        #   -p, --packages   interpret simple name arguments as packages
        return self.exec("undeploy", **kwargs)

    def get_project(self, directory, **kwargs):
        """
        Get the project metadata for the given directory
        """
        # The `doctl serverless get-metadata` command produces a JSON structure that summarizes the contents of a functions
        # project (a directory you have designated for functions development).  This can be useful for feeding into other tools.

        # Usage:
        #   doctl serverless get-metadata <directory> [flags]

        # Flags:
        #       --env string       Path to environment file
        #       --exclude string   Functions or packages to exclude
        #   -h, --help             help for get-metadata
        #       --include string   Function
        #       --no-triggers      Do not include triggers in the metadata
        return self.exec("get-metadata", directory, **kwargs)

    def watch_project(self, directory, **kwargs):
        # Type `doctl serverless watch <directory>` in a separate terminal window.  It will run until interrupted.
        # It will watch the directory (which should be one you initialized for serverless development) and will deploy
        # the contents to the cloud incrementally as it detects changes.

        # Usage:
        # doctl serverless watch <directory> [flags]

        # Flags:
        #     --apihost string     API host to use
        #     --auth string        OpenWhisk auth token to use
        #     --build-env string   Path to build-time environment file
        #     --env string         Path to runtime environment file
        #     --exclude string     Functions and/or packages to exclude
        # -h, --help               help for watch
        #     --include string     Functions and/or packages to include
        #     --insecure           Ignore SSL Certificates
        #     --remote-build       Run builds remotely
        #     --verbose-build      Display build details
        #     --verbose-zip        Display start/end of zipping phase for each function
        #     --yarn               Use yarn instead of npm for node builds
        return self.exec("watch", directory, **kwargs)

    def deploy_project(self, directory, **kwargs):
        # At any time you can use `doctl serverless deploy` to upload the contents of a functions project in your file system for
        # testing in your serverless namespace.  The project must be organized in the fashion expected by an App Platform Functions
        # component.  The `doctl serverless init` command will create a properly organized directory for you to work in.

        # Usage:
        #   doctl serverless deploy <directory> [flags]

        # Flags:
        #       --apihost string     API host to use
        #       --auth string        OpenWhisk auth token to use
        #       --build-env string   Path to build-time environment file
        #       --env string         Path to runtime environment file
        #       --exclude string     Functions and/or packages to exclude
        #   -h, --help               help for deploy
        #       --include string     Functions and/or packages to include
        #       --incremental        Deploy only changes since last deploy
        #       --insecure           Ignore SSL Certificates
        #       --remote-build       Run builds remotely
        #       --verbose-build      Display build details
        #       --verbose-zip        Display start/end of zipping phase for each function
        #       --yarn               Use yarn instead of npm for node builds

        return self.exec("deploy", directory, **kwargs)

    def get_function(self, function_name, **kwargs):
        # Use `doctl serverless functions get` to obtain the code or metadata of a deployed function.
        # This allows you to inspect the deployed copy and ascertain whether it corresponds to what
        # is in your functions project in the local file system.

        # Usage:
        #   doctl serverless functions get <functionName> [flags]

        # Flags:
        #       --code                   show function code (only works if code is not a zip file)
        #   -h, --help                   help for get
        #       --save                   save function code to file corresponding to the function name
        #       --save-as string         file to save function code to
        #   -E, --save-env string        save environment variables to FILE as key-value pairs
        #   -J, --save-env-json string   save environment variables to FILE as JSON
        #   -r, --url                    get function url
        # Get a function by name
        return self.exec("functions", "get", function_name, **kwargs)

    def run_function(self, function_name, **kwargs):
        # Use `doctl serverless functions invoke` to invoke a function in your functions namespace.
        # You can provide inputs and inspect outputs.

        # Usage:
        #   doctl serverless functions invoke <functionName> [flags]

        # Flags:
        #   -f, --full                wait for full activation record
        #   -h, --help                help for invoke
        #   -n, --no-wait             fire and forget (asynchronous invoke, does not wait for the result)
        #   -p, --param strings       parameter values in KEY:VALUE format, list allowed
        #   -P, --param-file string   FILE containing parameter values in JSON format
        #       --web                 Invoke as a web function, show result as web page
        return self.exec("functions", "invoke", function_name, **kwargs)

    def list_functions(self, **kwargs):
        # Use `doctl serverless functions list` to list the functions in your functions namespace.

        # Usage:
        #   doctl serverless functions list [<packageName>] [flags]

        # Aliases:
        #   list, ls

        # Flags:
        #       --count          show only the total number of functions
        #       --format         Columns for output in a comma-separated list. Possible values: `Update`, `Version`, `Runtime`, `Function`.
        #   -l, --limit string   only return LIMIT number of functions (default 30, max 200)
        #   -n, --name           sort results by name
        #       --name-sort      sort results by name
        #       --no-header      Return raw data with no headers
        #   -s, --skip string    exclude the first SKIP number of functions from the result
        return self.exec("functions", "list", **kwargs)

    def get_activation(self, activation_id=None, **kwargs):
        # Use `doctl serverless activations get` to retrieve the activation record for a previously invoked function.
        # There are several options for specifying the activation you want.  You can limit output to the result
        # or the logs.  The `doctl serverless activation logs` command has additional advanced capabilities for retrieving
        # logs.

        # Usage:
        # doctl serverless activations get [<activationId>] [flags]

        # Flags:
        # -f, --function string   Fetch activations for a specific function
        # -h, --help              help for get
        # -l, --last              Fetch the most recent activation (default)
        # -g, --logs              Emit only the logs, stripped of time stamps and stream identifier
        # -q, --quiet             Suppress last activation information header
        # -r, --result            Emit only the result
        # -s, --skip int          SKIP number of activations
        return self.exec("activations", "get", activation_id, **kwargs)

    def list_activations(self, function_name, **kwargs):
        # Use `doctl serverless activations list` to list the activation records that are present in the cloud for previously
        # invoked functions.

        # Usage:
        #   doctl serverless activations list [<function_name>] [flags]

        # Aliases:
        #   list, ls

        # Flags:
        #       --count       show only the total number of activations
        #       --format      Columns for output in a comma-separated list. Possible values: `Datetime`, `Status`, `Kind`, `Version`, `ActivationID`, `Start`, `Wait`, `Duration`, `Function`.
        #   -f, --full        include full activation description
        #   -h, --help        help for list
        #   -l, --limit int   only return LIMIT number of activations (default 30, max 200) (default 30)
        #       --no-header   Return raw data with no headers
        #       --since int   return activations with timestamps later than SINCE; measured in milliseconds since Th, 01, Jan 1970
        #   -s, --skip int    exclude the first SKIP number of activations from the result
        #       --upto int    return activations with timestamps earlier than UPTO; measured in milliseconds since Th, 01, Jan 1970
        return self.exec("activations", "list", function_name, **kwargs)

    def get_activation_logs(self, activation_id=None, **kwargs):
        # Use `doctl serverless activations logs` to retrieve the logs portion of one or more activation records
        # with various options, such as selecting by package or function, and optionally watching continuously
        # for new arrivals.

        # Usage:
        #   doctl serverless activations logs [<activationId>] [flags]

        # Flags:
        #       --follow            Fetch logs continuously
        #   -f, --function string   Fetch logs for a specific function
        #   -h, --help              help for logs
        #   -n, --limit int         Fetch the last LIMIT activation logs (up to 200) (default 1)
        #   -p, --package string    Fetch logs for a specific package
        #   -r, --strip             strip timestamp information and output first line only
        return self.exec("activations", "logs", activation_id, **kwargs)

    def get_activation_result(self, activation_id=None, **kwargs):
        # Use `doctl serverless activations result` to retrieve just the results portion
        # of one or more activation records.

        # Usage:
        #   doctl serverless activations result [<activationId>] [flags]

        # Flags:
        #   -f, --function string   Fetch results for a specific function
        #   -h, --help              help for result
        #   -l, --last              Fetch the most recent activation result (default)
        #   -n, --limit int         Fetch the last LIMIT activation results (default 30, max 200) (default 1)
        #   -q, --quiet             Suppress last activation information header
        #   -s, --skip int          SKIP number of activations
        return self.exec("activations", "result", activation_id, **kwargs)



if __name__ == "__main__":
    from pprint import pprint
    do_fx = DoFunctionsAPI()
    do_sls = DoServerlessAPI()
    pprint(do_sls.install())
    pprint(do_sls.upgrade())
    pprint(do_sls.connect())
    pprint(do_sls.list_functions())
    pprint(do_fx.list_namespaces())


