import os
from setuptools import setup, find_packages
from dodopy.__version__ import VERSION

reqs = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "requirements.txt")
)
with open(reqs) as f:
    install_requires = [req.strip().split("==")[0] for req in f]

config = {
    "name": "dodopy",
    "version": VERSION,
    "packages": find_packages(),
    "install_requires": install_requires,
    "author": "gltd",
    "author_email": "hey@gltd.email",
    "description": "A Python wrapper for Digital Ocean API Functions",
    "url": "http://gitlab.com/gltd/dodopy",
}

setup(**config)
