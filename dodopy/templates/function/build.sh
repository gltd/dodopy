#!/bin/bash

set -ex

rm -rf virtualenv || true
mkdir -p virtualenv
virtualenv --without-pip virtualenv
pip install -r requirements.txt --target virtualenv/lib/python3.9/site-packages

set +ex
