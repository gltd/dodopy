# dodopy

`dodopy` is a Python package for building and running Digital Ocean functions in Python.

It's a wrapper around the Digital Ocean API, with convenience functions for building
the scaffolding for a function, and for running and testing it locally.

## Todo:

- [x] Generate directory of functions from jinja templates
- [x] Deploy generated directory of functions
- [ ] Invoke generated functions, passing in data/parameters/environment vars and fetching results / logs
- [ ]