import os
import shutil
import logging
from textwrap import dedent
from typing import Any, Dict, List, Optional

from jinja2 import Environment
import yaml

from dodopy.core import TEMPLATE_DIR, DoFunctionsAPI, DoServerlessAPI
from dodopy import settings

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

fx = DoFunctionsAPI()
sls = DoServerlessAPI()


# yaml helpers
class NoAliasDumper(yaml.SafeDumper):
    """
    A YAML dumper that will never emit aliases.
    """

    def ignore_aliases(self, data):
        return True


def write_yaml(filepath, data):
    """
    Write a yaml file
    """
    with open(filepath, "w") as f:
        yaml.dump(
            data,
            f,
            default_flow_style=False,
            sort_keys=False,
            indent=4,
            Dumper=NoAliasDumper,
        )


# template helpers

jinja = Environment()


def write_template(target_directory, template_dir, **kwargs):
    """
    Write a template to a directory
    """
    if os.path.exists(target_directory):
        shutil.rmtree(target_directory)
    os.makedirs(target_directory)
    for filename in os.listdir(template_dir):
        from_filepath = os.path.join(template_dir, filename)
        to_filepath = os.path.join(target_directory, filename)
        log.debug(
            f"Writing {to_filepath} to using template:{from_filepath} and kwargs: {kwargs}"
        )
        with open(from_filepath, "r") as f:
            template = jinja.from_string(f.read())
        with open(to_filepath, "w") as f:
            f.write(template.render(**kwargs))
        os.chmod(to_filepath, 0o777)

# etc
def get_safe_name(name):
    """
    Get a safe name for a function, package, or project
    """
    return name.replace("-", "_").replace(" ", "_").strip()



# base classes


class Trigger(object):
    def __init__(self, name: str, cron: str, body:Optional[Dict[str, Any]]=None):
        self.name = get_safe_name(name)
        self.cron = cron
        self.body = body

    @property
    def source_details(self):
        d = {}
        if self.cron:
            d["cron"] = self.cron
        if self.body:
            d["withBody"] = self.body
        return d

    @property
    def config(self):
        """
        The project.yml config for this trigger
        """
        return dict(
            name=self.name,
            sourceType="scheduler",
            sourceDetails=self.source_details,
        )


class Function(object):
    """
    Base class for all functions
    """

    TEMPLATE_DIR = os.path.join(TEMPLATE_DIR, "function")
    DEFAULT_CODE = dedent(
        """
    import logging
    log = logging.getLogger(__name__)
    logging.basicConfig(level=logging.INFO)

    def main(event, context):
        log.info('Hello World!')
        return {"success": True}
    """
    ).strip()

    def __init__(
        self,
        name: Optional[str] = None,
        binary: bool = False,
        web: bool = False,
        runtime: Optional[str] = None,
        description: Optional[str] = None,
        code=None,
        main="",
        timeout: int = 60 * 5 * 1000,  # 5 minutes
        logs: int = 256,  # number of log lines to store
        memory: int = 256,  # memory in MB
        requirements: List[str] = [],
        environment: Dict[str, Any] = {},
        parameters: Dict[str, Any] = {},
        annotations: Dict[str, Any] = {},
        triggers: List[Trigger] = [],
    ):
        self.name = get_safe_name(name or self.__class__.__name__)
        self.binary = binary
        self.web = web
        self.code = code or self.DEFAULT_CODE
        self.timeout = timeout
        self.logs = logs
        self.memory = memory
        self.main = main
        self.runtime = runtime or settings.DEFAULT_RUNTIME
        self.environment = environment
        self.parameters = parameters
        self.annotations = annotations
        self.description = description or self.__class__.__doc__
        self.triggers = triggers
        self.requirements = requirements or []

    @property
    def template_params(self):
        """
        Parameters to pass to the Function template
        """
        return dict(
            name=self.name,
            description=self.description,
            code=self.code,
            requirements=self.requirements,
        )

    @property
    def config(self):
        """
        The project.yaml config for this function
        """
        return {
            "name": self.name,
            "web": self.web,
            "binary": self.binary,
            "main": self.main,
            "runtime": self.runtime,
            "environment": self.environment,
            "parameters": self.parameters,
            "annotations": self.annotations,
            "limits": {
                "timeout": self.timeout,
                "memory": self.memory,
                "logs": self.logs,
            },
            "triggers": [t.config for t in self.triggers],
        }

    def write(self, function_dir, template_dir=None, **kwargs):
        # set kwargs to default values
        template_dir = template_dir or self.TEMPLATE_DIR
        kwargs = {**self.template_params, **kwargs}
        write_template(function_dir, template_dir, **kwargs)


class Package(object):
    """
    A package is a collection of Functions nested in a directory
    """

    TEMPLATE_DIR = os.path.join(TEMPLATE_DIR, "package")

    def __init__(
        self,
        project_dir,
        name=None,
        description=None,
        environment: Dict[str, Any] = {},
        parameters: Dict[str, Any]={},
        annotations: Dict[str, Any]={},
        functions: List[Function] = [],
    ):
        self.project_dir = project_dir
        self.name = get_safe_name(name or self.__class__.__name__)
        self.description = description or self.__class__.__doc__
        self.environment = environment
        self.parameters = parameters
        self.annotations = annotations
        self.functions = functions

    @property
    def template_params(self):
        """
        Parameters to pass to the package template
        """
        return dict(
            name=self.name,
            description=self.description,
            functions=[f.template_params for f in self.functions],
        )

    @property
    def config(self) -> Dict[str, Any]:
        """
        The project.yaml config for this package
        """
        return {
            "name": self.name,
            "environment": self.environment,
            "parameters": self.parameters,
            "annotations": self.annotations,
            "functions": [f.config for f in self.functions],
        }

    def write_functions(self, package_dir, template_dir) -> None:
        """
        Write all functions to the package directory
        """
        for function in self.functions:
            function_dir = os.path.join(package_dir, function.name)
            function.write(function_dir, template_dir)

    def write(self, package_dir=None, template_dir=None, **kwargs) -> None:
        """
        Write the package to the project directory
        """
        if not package_dir:
            package_dir = os.path.join(self.project_dir, "packages", self.name)
        kwargs = {**self.template_params, **kwargs}
        self.write_functions(package_dir, template_dir)


class Project(object):
    """
    A project is a collection of packages nested in a directory
    with project.yml file
    """

    TEMPLATE_DIR = os.path.join(TEMPLATE_DIR, "project")

    def __init__(
        self,
        project_dir,
        name=None,
        description=None,
        environment: Dict[str, Any] = {},
        parameters: Dict[str, Any] = {},
        annotations: Dict[str, Any] = {},
        packages: List[Package] = [],
    ):
        self.project_dir = project_dir
        self.name = get_safe_name(name or self.__class__.__name__)
        self.description = description or self.__class__.__doc__
        self.environment = environment
        self.parameters = parameters
        self.annotations = annotations
        self.packages = packages
        self.fx_name_lookup = {
            f.name: {"name": f"{p.name}/{f.name}", "function": f}
            for p in self.packages for f in self.functions
        }

    def _fx_name(self, name):
        if "/" in name:
            return name
        try:
            return self.function_name_lookup[name]["name"]
        except KeyError:
            raise KeyError(f"Function {name} not found in project")

    @property
    def template_params(self) -> Dict[str, Any]:
        """
        Parameters to pass to the project template
        """
        return dict(
            name=self.name,
            description=self.description,
            packages=[package.template_params for package in self.packages],
        )

    @property
    def config(self) -> Dict[str, Any]:
        """
        Write this project to a yaml dict, following the DO specification for
        the project.yml file
        """
        return {
            "environment": self.environment,
            "parameters": self.parameters,
            "packages": [p.config for p in self.packages],
        }

    def write_config(self) -> None:
        """
        Write the project.yml file
        """
        filepath = os.path.join(self.project_dir, "project.yml")
        write_yaml(filepath, self.config)

    def write_packages(self) -> None:
        """
        Write all packages to the project directory
        """
        # write package directories
        for package in self.packages:
            package_dir = os.path.join(self.project_dir, "packages", package.name)
            os.makedirs(package_dir, exist_ok=True)
            package.write(package_dir)

    def write(self, template_dir=None, **kwargs) -> None:
        """
        Write the project to the project directory
        """
        template_dir = template_dir or self.TEMPLATE_DIR
        kwargs = {**self.template_params, **kwargs}
        write_template(self.project_dir, template_dir, **kwargs)
        self.write_config()
        self.write_packages()

    def deploy(self, **kwargs):
        """
        Deploy the project to the DO platform
        """
        return sls.deploy_project(self.project_dir, **kwargs)



if __name__ == "__main__":
    shutil.rmtree("tmp", ignore_errors=True)
    functions = [
        Function(
            name="test-fx-3",
            description="test function 3",
            triggers=[Trigger(name="test-trigger-1", cron="* * * * *")],
            requirements=["numpy"],
        ),
        Function(name="test-fx-4", description="test function 4", requirements=["numpy"]),
    ]

    package = Package(
        project_dir="tmp", name="test", description="test package", functions=functions
    )
    project = Project(
        project_dir="tmp", name="test", description="test project", packages=[package]
    )
    project.write()
    project.deploy()
    project.run_function("test/test-fx-3", {"a": 1})
