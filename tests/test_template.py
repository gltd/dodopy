import os
import shutil
import yaml
from dodopy.base import Function, Trigger, Package, Project

here = os.path.dirname(os.path.abspath(__file__))
tmp_dir = os.path.join(here, "tmp")

def test_template():
    project_yml_path = os.path.join(tmp_dir, "project.yml")
    functions = [
        Function(name="test-fx-3", description="test function 3", triggers=[Trigger(name="test-trigger-1", cron="* * * * *")]),
        Function(name="test-fx-4", description="test function 4"),
    ]

    package = Package(
        project_dir=tmp_dir, name="test", description="test package", functions=functions
    )
    project = Project(
        project_dir=tmp_dir, name="test", description="test project", packages=[package]
    )
    project.write()
    with open(project_yml_path, "r") as f:
        config = yaml.safe_load(f)
        shutil.rmtree(tmp_dir)
        assert config == {'name': 'test', 'environment': {}, 'parameters': {}, 'annotations': {}, 'packages': [{'name': 'test', 'environment': {}, 'parameters': {}, 'annotations': {}, 'functions': [{'name': 'test-fx-3', 'web': False, 'binary': False, 'main': '', 'runtime': 'python:default', 'environment': {}, 'parameters': {}, 'annotations': {}, 'limits': {'timeout': 300000, 'memory': 256, 'logs': 256, 'concurrency': 1}, 'triggers': [{'name': 'test-trigger-1', 'sourceType': 'scheduler', 'sourceDetails': {'cron': '* * * * *'}}]}, {'name': 'test-fx-4', 'web': False, 'binary': False, 'main': '', 'runtime': 'python:default', 'environment': {}, 'parameters': {}, 'annotations': {}, 'limits': {'timeout': 300000, 'memory': 256, 'logs': 256, 'concurrency': 1}, 'triggers': []}]}]}
