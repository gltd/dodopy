import os
from dotenv import load_dotenv
load_dotenv()

# SECURITY WARNING: keep the secret key used in production secret!
ACCESS_TOKEN = os.getenv('DIGITALOCEAN_ACCESS_TOKEN')
BASE_URL = 'https://api.digitalocean.com/v2/'
DOCTL_EXECUTABLE = os.getenv('DOCTL_EXECUTABLE', '/usr/local/bin/doctl')
DEFAULT_RUNTIME = "python:default"
